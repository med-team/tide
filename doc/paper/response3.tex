\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage[dvips]{color}
\usepackage{url}
\usepackage{xr}                    % Allow cross-references between docs.

\newcommand{\breview}{\begin{quotation}\begin{em}\noindent}
\newcommand{\ereview}{\end{em}\end{quotation}}

% Allow references to the supplementary tables and figures.
\externaldocument{tide}
\externaldocument{tide-supplement}

\begin{document}

\hspace*{3.0in}\today

\vspace*{3ex}

\noindent
Dear Dr.\ Weintraub:

\vspace*{1ex}

We are submitting a revised version of the manuscript ``Faster SEQUEST
Searching for Peptide Identification from Tandem Mass Spectra''
(pr-2010-01196n.R1) in which we address all of the reviewers'
concerns. Per your instructions, we have included a marked up copy
showing the differences between the previous submission and the
current one.

Please note that, in addition to the changes requested by the
reviewer, we have made two additional minor modifications.  First, we
eliminated the following text from page 7 because it repeated ideas
from a paragraph on page 6:
\begin{quotation}
``For most applications, the database of proteins, from which
  candidate peptides are derived, is considered to change
  infrequently. As a consequence, an arbitrary amount of
  precomputation may be performed on the peptide set before input
  spectra are to be analyzed.  Several recently developed search
  tools, including TurboSEQUEST, Crux and Tide, take advantage of this
  opportunity by indexing the peptide set by precursor mass ahead of
  time.''
\end{quotation}
Second, we have rewritten a paragraph of the text to reflect the fact
that, since our last submission, we have informally investigated the
nature of the minor differences between Crux and SEQUEST and found
them to be inconsequential.  Here is the old version of the paragraph:
\begin{quotation}
``Tide's precise fidelity to Crux's scoring introduced
constraints on the approach to optimization that would not have
existed had Tide's output been allowed to vary slightly from
Crux's. Conversely, because Crux's output is less faithful to
SEQUEST's output, some of Tide's optimizations may only be possible
because Crux and Tide differ from SEQUEST. We have not undertaken to
investigate all of these differences, because they were shown
[4] to have little overall effect on accuracy and
because such differences arise even among various versions of SEQUEST
(Supplementary Figure~\ref{figure:scatter}).  Nevertheless, at this
point we cannot be certain that all of Tide's speed would be preserved
if we eliminated all scoring differences between Tide and a specific
version of SEQUEST.''
\end{quotation}
And here is the new version:
\begin{quotation}
``Tide's precise fidelity to Crux's scoring introduced constraints on
  the approach to optimization that would not have existed had Tide's
  output been allowed to vary slightly from Crux's. On the other hand,
  Crux's output is less faithful to SEQUEST's output. Minor scoring
  differences arise even among various versions of SEQUEST
  (Figure~\ref{figure:scatter}), and were shown [4] to have little
  overall effect on accuracy. An informal investigation showed that
  the small differences between Crux and SEQUEST were mostly due to
  minor bugs in one or the other program, and that such differences
  have no impact on Tide's speed.''
\end{quotation}

As requested, we are submitting two versions of the manuscript, one of
which indicates the changes that we made relative to the previous
version.  We hope that the revised manuscript is suitable for
publication in the {\em Journal of Proteome Research}.

\vspace*{1ex}

\noindent
Sincerely,

\hspace*{1ex}

\noindent
William Stafford Noble\\
Department of Genome Sciences\\
University of Washington

\clearpage
\section*{Reviewer \#2}

\breview While my first five comments of the original draft were
addressed sufficiently, the last five were not addressed in the text
or acknowledged in the authors rebuttal.
\ereview

The authors regret this omission in our last submission and offer our
apology. We address the reviewer's specific concerns here.

\breview 1) At the beginning of the series of optimizations discussed
on page 8, there should be some indication that the order of the
optimizations has to do with when they were implemented, rather than
how they are related.
\ereview

The first mention of Table 1 in the text has been changed to say:

Table~\ref{table:timing} shows timing results in the actual order
these changes were introduced to Tide, with later optimizations often
building upon earlier ones.

We have also updated the caption to Table 1 adding the following:

Optimizations in the order they were introduced into Tide, and the
measured improvement introduced by each. In several cases, earlier
optimizations were prerequisite for later ones: e.g. line 9 showed a
slight degradation, but was prerequisite for the gain in line 11.

\breview The optimizations should be numbered (as corresponding to the
supplemental) and the numbers should be indicated clearly in Table 1.
Referencing all three at the same time (the manuscript, supplemental,
and the table) can be confusing because the indicator titles don't
always match Table 1.  \ereview

We have added to Table 1 specific references to the body of the text
or supplement to clarify the correspondence between the Table and the
text.

\breview 2) Bullet ``Making the theoretical peaks vector five-fold
sparser'' on page 8 should be clarified in the manuscript.  Even a
simple sentence saying that ``B/Y-H2O, B/Y-NH3, and B-CO peaks are not
stored, but added to the theoretical spectrum on the fly'' would do
wonders to clarify this optimization.
\ereview

We have clarified the paragraph in question and referred the reader to
the appropriate section of the supplement for details:

Theoretical spectra in the SEQUEST algorithm occur in groups
corresponding to cleavage events, with somewhat predictable spacing
among the peaks within a group. Tide takes advantage of such peak
groupings to represent the complete set of theoretical peaks even more
sparsely. This is done by adding together the peaks in a group as part
of the spectrum preprocessing step. Details are given in
Section~\ref{subsubsection:fivefold} of the supplement.

\breview 3) The ``FIFO memory allocator'' bullet on page 9 is pretty
vague and the supplemental does little to clarify.
\ereview

We have added an explanatory paragraph to
Section~\ref{subsubsection:fifo} of the supplement, as follows:

The built-in memory allocator typical of most C and C++ programs makes
no assumptions on the order in which blocks of memory will be
allocated and deallocated. It therefore needs to keep careful track of
which blocks of memory are in use at any given time. Updating this
information as a program runs can take significant time in case many
allocation and deallocation operations are performed, as is the case
in Tide. However, in Tide, the pattern of allocations and
deallocations closely mirrors the enqueuing and dequeueing of
candidates in the active peptide queue, described in
Section~\ref{subsubsection:join}. This first-in-first-out (FIFO)
memory usage pattern is entirely predictable: if memory block $A$ is
allocated before memory block $B$, then $A$ will be freed before
$B$. This greatly simplifies the bookkeeping usually associated with
the built-in memory allocator such that the memory needed by Tide can,
for the most part, be kept in a contiguous region of machine
memory. Therefore, we introduced in Tide a specialized
first-in-first-out memory allocator that performs well on data
allocated according to the FIFO usage pattern.

\breview 4) Revision 9 in Table 1 indicates that ``Sparse difference
vector representation'' increased the average runtime.  Why was this
``optimization'' kept?
\ereview

We have added an explanatory paragraph near the end of
Section~\ref{subsection:optimizations} as follows:

The sparse difference vector representation, shown in line 9 of
Table~\ref{table:timing}, introduced a slight time penalty, but was
implemented as a prerequisite for storing the sparse vector
differences to disk (line 11 of Table~\ref{table:timing}). The two
changes taken together performed very well.

\breview 5) Supplemental Figure 1 is actually quite compelling, if
only for the reason that it shows just how different the 1993 and 2009
versions of SEQUEST are, indicating that the Tide vs SEQUEST scoring
differences are relatively unimportant.  I recommend moving this
figure into the main text.
\ereview

We have taken the reviewer's suggestion and moved Supplemental Figure
1 to the main text. By way of explanation, we added the following to
the body of the text:

Figure~\ref{figure:scatter} compares the XCorr scores computed by
Tide against those computed by two different versions of
SEQUEST. These scoring differences between Tide and SEQUEST are no
bigger than those between the two SEQUEST versions, which are compared
at the bottom of the figure.

\breview In addition, I'd like to make the following suggestions upon
re-reading:
\ereview

\breview 1) in 2007 Nathan Edwards suggested another database
compression strategy to reference in page 1, paragraph 6: Edwards
NJ. ``Novel peptide identification from tandem mass spectra using ESTs
and sequence database compression.'' Mol Syst Biol. 2007;3:102.
\ereview

We thank the reviewer for this additional reference. We have added it
to the following paragraph, which references memory and compression
issues and includes an earlier Edwards citation as well.

\breview 2) Perhaps the most compelling reason for optimizing database
searching is to enable ``on the fly'' analysis of MS/MS data as it is
being acquired by the instrument.  Are you currently considering this
usage?  Should it be mentioned as follow-up in the concluding
paragraph on page 16?
\ereview

The authors thank the reviewer for this suggestion, which has been
included in the concluding paragraph:

Tide also opens the possibility of full peptide database search in
real time, as spectra are acquired by the instrument.

\end{document}
